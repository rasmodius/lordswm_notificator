from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import chime
from plyer import notification
import linecache
import tkinter as tk
import time

# Instantiate a web driver (assuming you have downloaded and set up the appropriate driver for your browser)
chrome_options = Options()
chrome_options.add_argument("--headless")  # Enable headless mode



def get_value():
    global username
    username = userentry.get()
    global password
    password = passentry.get()

def notificator():
    # Instantiate a web driver with Chrome options
    driver = webdriver.Chrome(options=chrome_options)

    # Navigate to the login page
    driver.get("https://www.lordswm.com")

    try:
        # Locate the username and password input fields and the login button
        username_field = driver.find_element(By.NAME, "login")
        password_field = driver.find_element(By.NAME, "pass")
        login_button = driver.find_element(By.CLASS_NAME, "entergame")

        # Input your login credentials
        username_field.send_keys(username)
        password_field.send_keys(password)


        # Click the login button
        login_button.click()


        while True:
            driver.get("https://www.lordswm.com/home.php")
            element = driver.find_element(By.CLASS_NAME, "wbwhite")
            try:
                button = driver.find_element(By.LINK_TEXT, "Got it!")
                text = element.text
                if "Got it!" in text:
                    print("New game info")
                    chime.success()
                    notification.notify("New game info", "There is a new info in the game. Check it out.", "LordsWM")
                    button.click()
            except:
                print("No new game info")
                break
        # Go to guild page
        driver.get("https://www.lordswm.com/mercenary_guild.php")

        element = driver.find_element(By.CLASS_NAME, "wbwhite")
        text = element.text

        # Play tone if there is an availabe quest
        if ("The Guild has a quest for you." in text) and ("You are in a different location." in text):
            print("Guild Quest available")
            chime.success()
            notification.notify("New quest", "There is a new quest at the mercenary guild! Go to the mercenary guild location.", "LordsWM")
        elif (not "Sorry, the Guild has no quests for you right now" in text) and (not "You are in a different location." in text):
            print("Guild Quest available")
            chime.success()
            notification.notify("New mercenary guild quest", "There is a new quest at the mercenary guild!", "LordsWM")
        else:
            print("No quest")

        # Go to map
        driver.get("https://www.lordswm.com/map.php")

        # Play tone if there is an availabe hunting
        try:
            element = driver.find_element(By.ID, "next_ht_new")
            print("No hunting available")
        except:
            print("New hunting available")
            chime.success()
            notification.notify("Hunting", "New hunting available", "LordsWM")

        # Go to inventory
        driver.get("https://www.lordswm.com/inventory.php")

        # Play tone if repairing has finished
        try:
            element = driver.find_element(By.CLASS_NAME, "inventory_repair_text")
            print("An item is repairing")
        except:
            print("No items repairing")
            chime.success()
            notification.notify("Smiths' guild", "You can repair new items", "LordsWM")

        # Go to character page
        driver.get("https://www.lordswm.com/home.php")
        element = driver.find_element(By.CLASS_NAME, "wbwhite")
        element = driver.find_element(By.XPATH, "//table[@class='wb']/tbody/tr[4]/td[@class='wbwhite']")
        text = element.text
        if "You are currently unemployed." in text:
            print("You are currently unemloyed")
            chime.success()
            notification.notify("Laborers' guild", "You are currently unemployed", "LordsWM")
        else:
            print("You are already employed")
    except:
        print("Click Submit button:")
    driver. close()
    window.after(300000, notificator)

window = tk.Tk()
window.title("LordsWM Notificator")
title = tk.Label(text="LordsWM")
title.pack()
decs = tk.Label(text="This program checks the guilds every 5 minutes")
decs.pack()
userlabel = tk.Label(text="Username:")
userlabel.pack()
userentry = tk.Entry(width=50)
userentry.pack()
passlabel = tk.Label(text="Password:")
passlabel.pack()
passentry = tk.Entry(show="*", width=50)
passentry.pack()
button = tk.Button(window, text="Submit", command=lambda:[get_value(), notificator(), button.destroy()])
button.pack()
exitbutton = tk.Button(window, text="Exit", command=window.quit)
exitbutton.pack()
window.after(60000, notificator)
window.mainloop()
